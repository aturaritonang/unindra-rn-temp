import React, { Component } from 'react';
import { Text, TextInput, View, Button, Switch } from 'react-native';
// import CheckBox from '@react-native-community/checkbox';

class CategoryFrom extends Component {
	constructor(props) {
		super(props);
		this.state = {
			...this.props,
		};
	}

	render() {
		const { styles, item, handleChange, handleChangeCheckBox } = this.props;
		return (
			<View>
				<Text style={styles.title}>Form Category</Text>
				<Text style={styles.inputLabel}>Initial:</Text>
				<TextInput
					value={item.initial}
					onChangeText={text => handleChange('initial', text)}
					style={{
						height: 40,
						borderColor: 'gray',
						borderWidth: 1,
					}}
				/>
				<Text style={styles.inputLabel}>Name:</Text>
				<TextInput
					value={item.name}
					onChangeText={text => handleChange('name', text)}
					style={{
						height: 40,
						borderColor: 'gray',
						borderWidth: 1,
					}}
				/>
				<View style={{ flexDirection: 'row' }}>
					<Switch
						style={{ marginTop: 4 }}
						trackColor={{ false: '#767577', true: '#81b0ff' }}
						thumbColor={item.active ? '#f5dd4b' : '#f4f3f4'}
						ios_backgroundColor="#3e3e3e"
						onValueChange={val => {
							handleChangeCheckBox('active');
						}}
						value={item.active}
					/>

					<Text style={{ paddingTop: 8, paddingLeft: 4 }}>
						{item.active ? 'Activated' : 'Inactivated'}
					</Text>
				</View>
			</View>
		);
	}
}

export default CategoryFrom;
