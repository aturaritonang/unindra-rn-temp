import React, { Component } from 'react';
import { Text, View, ScrollView, Button, Alert, Modal, Pressable } from 'react-native';

import CategoryFrom from './form.js';

import { CategoryService } from '../../services/categoryService';

class Category extends Component {
	constructor(props) {
		super(props);
		this.state = {
			list: [
				{
					_id: '1',
					initial: 'A',
					name: 'Aaa Aaaa',
					active: true,
				},
				{
					_id: '2',
					initial: 'B',
					name: 'Bbbb Bbbb',
					active: false,
				},
				{
					_id: '3',
					initial: 'C',
					name: 'Cccc Cccc',
					active: true,
				},
			],
			item: {
				initial: '',
				name: '',
				active: true,
			},
			modalVisible: false,
			...this.props,
		};
	}

	componentDidMount() {
		this.loadCategories();
	}

	loadCategories = async () => {
		const cats = await CategoryService.all();
		if (cats.success) {
			this.setState({
				list: cats.result,
			});
		} else {
			alert('Error');
		}
	};

	setModalVisible() {
		this.setState({
			modalVisible: !this.state.modalVisible,
		});
	}

	createNew() {
		this.setState({
			item: {
				initial: '',
				name: '',
				active: true,
			},
		});
		this.setModalVisible();
	}

	handleChange = (name, value) => {
		this.setState({
			item: {
				...this.state.item,
				[name]: value,
			},
		});
	};

	handleChangeCheckBox = name => {
		this.setState({
			item: {
				...this.state.item,
				[name]: !this.state.item.active,
			},
		});
	};

	submit = async () => {
		const cats = await CategoryService.post(this.state.item);
		if (cats.success) {
			this.loadCategories();
			this.setState({
				modalVisible: false,
			});
		} else {
			alert('Error');
		}
	};

	render() {
		const { styles, list, item, modalVisible } = this.state;
		return (
			<View>
				<Modal
					animationType="slide"
					visible={modalVisible}
					onRequestClose={() => {
						setModalVisible();
					}}
				>
					<View style={{ padding: 20 }}>
						<View style={styles.modalView}>
							<CategoryFrom
								styles={styles}
								item={item}
								handleChange={this.handleChange}
								handleChangeCheckBox={this.handleChangeCheckBox}
							/>
						</View>
					</View>
					<View
						style={{
							flexDirection: 'row',
							height: 40,
							padding: 4,
						}}
					>
						<View style={{ flex: 0.5, paddingRight: 4 }}>
							<Button title="Save" onPress={() => this.submit()} />
						</View>
						<View style={{ flex: 0.5, paddingLeft: 4 }}>
							<Button title="Close" onPress={() => this.setModalVisible()} />
						</View>
					</View>
				</Modal>
				<ScrollView>
					<Text style={styles.title}>Daftar Category</Text>
					<Button title="Create New" onPress={() => this.createNew()} />
					{/* <Text>
						{JSON.stringify(item)}
					</Text> */}
					<View
						style={{
							flexDirection: 'row',
							height: 40,
							padding: 4,
						}}
					>
						<View style={{ flex: 0.3 }}>
							<Text style={styles.header}>Initial</Text>
						</View>
						<View style={{ flex: 0.5 }}>
							<Text style={styles.header}>Name</Text>
						</View>
						<View style={{ flex: 0.2 }}>
							<Text style={styles.header}>Active</Text>
						</View>
					</View>
					<View>
						{list.map(item => {
							return (
								<View
									key={item._id}
									style={{
										flexDirection: 'row',
										height: 40,
										padding: 4,
									}}
								>
									<View style={{ flex: 0.3 }}>
										<Text>
											{item.initial}
										</Text>
									</View>
									<View style={{ flex: 0.5 }}>
										<Text>
											{item.name}
										</Text>
									</View>
									<View style={{ flex: 0.2 }}>
										<Text>
											{item.active ? 'True' : 'False'}
										</Text>
									</View>
								</View>
							);
						})}
					</View>
				</ScrollView>
			</View>
		);
	}
}

export default Category;
