import React, { Component } from 'react';
import { Text, View } from 'react-native';

import Room from './room';

class Home extends Component {
	constructor(props) {
		super(props);
	}

	teras(posisi) {
		return (
			<Text>
				Ini adalah teras {posisi}
			</Text>
		);
	}

	render() {
		const { styles, nama } = this.props;
		return (
			<View>
				<Text style={styles.red}>
					Ini {this.props.nama} dari home!
				</Text>
				<View>
					{this.teras('Depan')}
					{this.teras('Belakang')}
				</View>
				<Room nama={nama} anak="Iwan" styles={styles} />
			</View>
		);
	}
}

export default Home;
