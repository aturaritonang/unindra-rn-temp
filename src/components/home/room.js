import React, { Component } from 'react';
import { View, Text } from 'react-native';

class Room extends Component {
	constructor(props) {
		super(props);
		// this.state = this.props;
	}

	render() {
		const { nama, anak, styles } = this.props;
		return (
			<Text style={styles.green}>
				Ini kamar anaknya {nama} yg bernama {anak}
			</Text>
		);
	}
}

export default Room;
