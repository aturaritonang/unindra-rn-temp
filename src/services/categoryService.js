import { config } from '../base/config';
import axios from 'axios';

export const CategoryService = {
	all: () => {
		const result = axios
			.get(config.apiUrl + '/category', {
				headers: config.headers,
			})
			.then(respons => {
				return {
					success: true,
					result: respons.data,
				};
			})
			.catch(error => {
				return {
					success: false,
					result: error,
				};
			});
		return result;
	},
	post: category => {
		const result = axios
			.post(config.apiUrl + '/category', category, { headers: config.headers })
			.then(respons => {
				return {
					success: true,
					result: respons.data.result,
				};
			})
			.catch(error => {
				return {
					success: false,
					respons: error,
				};
			});
		return result;
	},
};
